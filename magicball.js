const respostas = [
    'Certeza.',
    'É decididamente que sim.',
    'Sem dúvida.',
    'Sim definitivamente.',
    'Você pode contar com isto.',
    'A meu ver, sim.',
    'Provavelmente.',
    'Boa perspectiva.',
    'Sim.',
    'Sinais apontam que sim.',
    'Resposta nebulosa, tente novamente.',
    'Pergunte novamente mais tarde.',
    'Melhor não te dizer agora.',
    'Não é possível prever agora.',
    'Concentre-se e pergunte novamente.',
    'Não conte com isso.',
    'Minha resposta é não.',
    'Minhas fontes dizem não.',
    'Sem boa perspectiva.',
    'Muito duvidoso.', 
]


const oito = document.getElementById('oito')
const action = document.getElementById('bola')
const pergunta = document.getElementById('pergunta')
const resposta = document.getElementById('resposta')


pergunta.addEventListener('click', function() {
    oito.innerHTML = '8'
    resposta.innerHTML = ''
})

action.addEventListener('click', function() {
    if (pergunta.value === '') {
        alert('Faça uma pergunta!')
    } else {
        oito.innerHTML = ''
        let numero = Math.ceil(Math.random() * 20)
        resposta.innerHTML = respostas[numero]
    }
})

